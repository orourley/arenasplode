import { App } from 'elm-app';
import { Birch } from 'birch';
import { Level } from 'level';
import { Player } from 'player';
import { Entity } from 'entities/entity';
import { EntityFactory } from 'entity-factory';
import { Item } from 'entities/item';
import { Stick } from 'entities/stick';

export class ArenaSplodeApp extends App {
	/** Destructs the app. */
	override destroy(): void {
		for (const entity of this._entities) {
			entity.destroy();
		}
		if (this._level !== undefined) {
			this._level.destroy();
		}
		window.removeEventListener('resize', this._adjustViewports);
		if (this._birch !== undefined) {
			this._birch.destroy();
		}
	}

	/** Gets the Birch engine. */
	get birch(): Birch.Engine {
		return this._birch;
	}

	/** Gets the scene. */
	get scene(): Birch.Render.Scene {
		return this._scene;
	}

	/** Gets the level. */
	get level(): Level {
		return this._level;
	}

	/** Gets the players list. */
	get players(): Birch.FastMapReadonly<number, Player> {
		return this._players;
	}

	/** Gets the player. */
	getPlayer(index: number): Player | undefined {
		return this._players.get(index);
	}

	/** Adds an entity so that it will update. */
	addEntity(entity: Entity): void {
		this._entities.add(entity);
		if (entity instanceof Item && !(entity instanceof Stick)) {
			this._numItems += 1;
		}
	}

	/** Removes an entity so that it will stop updating. */
	removeAndDestroyEntity(entity: Entity): void {
		this._entitiesToRemove.add(entity);
	}

	getEntities(): Birch.FastIterable<Entity> {
		return this._entities;
	}

	/** Shows the win screen for the given player. */
	showWin(playerIndex: number): void {
		// Remove all entities in the level.
		for (const entity of this._entities) {
			this._entitiesToRemove.add(entity);
		}
		// Get the character name.
		const player = this._players.get(playerIndex)!;
		const name = player.character!.name;
		// Destroy all characters.
		for (const entry of this._players) {
			entry.value.somePlayerWon();
		}
		// Show the win screen.
		this.query('.win', HTMLDivElement)!.style.display = 'block';
		// Show the character big.
		if (name.startsWith('blob')) {
			this.query('.win .character', HTMLImageElement)!.src = name;
		}
		else {
			this.query('.win .character', HTMLImageElement)!.src = 'assets/sprites/' + name + '.png';
		}
		// Play the win sound.
		const winSound = this._birch.sounds.get(`win`);
		winSound.play();
		this._birch.sounds.release(winSound);
	}

	/** Restarts the game. */
	restart(): void {
		// Remove any entities that need to be removed.
		for (const entity of this._entities) {
			entity.destroy();
		}
		this._entities.clear();
		this._numItems = 0;
		this._announcerTime = 0;

		// Reset the screens.
		this.query('.win', HTMLDivElement)!.style.display = '';
		this.query('.message', HTMLDivElement)!.style.display = '';

		// Setup the waiting for players message.
		this.setHtml(`<span>Waiting for Players...</span>`, this.query('.message', HTMLDivElement)!);

		// Clear out the players.
		for (const entry of this._players) {
			entry.value.destroy();
		}
		this._players.clear();

		// Add the callback for when controllers are connected or disconnected.
		this._birch.input.setControllerActiveCallback((index: number, active: boolean) => {
			if (active) {
				// If the 'Waiting for player' message is there, turn it off.
				const messageElem = this.query('.message', HTMLDivElement);
				if (messageElem !== undefined) {
					messageElem.style.display = 'none';
				}
				console.log(`Player ${index + 1} is active.`);
				const oldPlayer = this._players.get(index);
				// Clean out any old player if it's still there.
				if (oldPlayer !== undefined) {
					oldPlayer.destroy();
				}
				// Add new player.
				this._players.set(index, new Player(index, this));
			}
			else {
				console.log(`Player ${index + 1} is inactive.`);
				const player = this._players.get(index);
				if (player !== undefined) {
					player.destroy();
					this._players.remove(index);
				}
			}
			this._adjustViewports();
		});
	}

	/** Gets the paths to all of the character sprites. */
	get characterSpritePaths(): string [] {
		return this._characterSpritePaths;
	}

	/** Called when the user drops a folder of characters into the app. */
	private _dragAndDropCharacters(event: DragEvent): void {
		// Process every character.
		if (event.dataTransfer !== null) {
			var items = event.dataTransfer.items;
			for (var i = 0; i < items.length; i++) {
				// webkitGetAsEntry is where the magic happens
				var item = items[i].webkitGetAsEntry();
				if (item !== null && item.isDirectory) {
					this._traverseFileTree(item, undefined);
				}
			}
		}
		// Make the drag-and-drop go green.
		this.query('.custom-characters', HTMLDivElement)!.style.backgroundColor = 'darkgreen';
		// Prevent anything else the browser might do.
		event.preventDefault();
	}

	/** Used by _dragAndDropCharacters() to process the dropped files. */
	private _traverseFileTree(item: FileSystemEntry, path: string | undefined): void {
		path = path || "";
		if (item.isFile) {
			// If the file is an image, add it to the list of character sprite paths.
			(item as FileSystemFileEntry).file((file: File) => {
				if (['png', 'PNG', 'jpg', 'JPG', 'gif', 'GIF'].includes(file.name.substr(file.name.lastIndexOf('.') + 1))) {
					const blobUrl = URL.createObjectURL(file);
					this._characterSpritePaths.push(blobUrl);
				}
			});
		}
		else if (item.isDirectory) {
			// if the file is a directory, recurse further down.
			var dirReader = (item as FileSystemDirectoryEntry).createReader();
			dirReader.readEntries((entries) => {
				for (var i = 0; i < entries.length; i++) {
					this._traverseFileTree(entries[i], path + item.name + "/");
				}
			});
		}
	}

	/** Prevent the drag-and-drop from just navigating to the folder dropped in. */
	private _dragOverCharacters(event: Event): void {
		event.preventDefault();
	}

	/** Setup the default characters if no custom character paths are added. */
	private async _setupDefaultCharacterSpritePaths(): Promise<void> {
		const text = await fetch('assets/sprites/characters/list.txt').then(response => response.text());
		const sprites = text.split('\n');
		for (const sprite of sprites) {
			if (sprite !== '') {
				this._characterSpritePaths.push(`characters/${sprite}`);
			}
		}
	}

	/** Updates the game. */
	private _update(deltaTime: number): void {
		// Remove any entities that need to be removed.
		for (const entity of this._entitiesToRemove) {
			if (this._entities.remove(entity)) {
				if (entity instanceof Item && !(entity instanceof Stick)) {
					this._numItems -= 1;
				}
				entity.destroy();
			}
		}

		// Check if there enough items in the world.
		while (this._numItems < this._players.size * this._level.size.x * this._level.size.y / 240) {
			this._entityFactory.createRandomItem();
		}

		// Do the update for every player.
		for (const entry of this._players) {
			entry.value.updateControls(deltaTime);
		}

		// Check the state of the game to see if we're playing.
		let canPlay = this._players.size > 0;
		for (let entry of this._players) {
			if (entry.value.mode !== 'play') {
				canPlay = false;
			}
		}

		if (canPlay) {
			// Play the announcer, possibly.
			if (this._announcerTime === 0) {
				const announcerSound = this._birch.sounds.get(`go`);
				announcerSound.play();
				this._birch.sounds.release(announcerSound);
				this._announcerTime = Date.now() / 1000;
			}
			else if (Math.random() < 0.05 * deltaTime && Date.now() / 1000 - this._announcerTime >= 20) {
				const i = Math.floor(Math.random() * 4);
				// Play the announcer sound.
				const announcerSound = this._birch.sounds.get(`announcer${i}`);
				announcerSound.play();
				this._birch.sounds.release(announcerSound);
				this._announcerTime = Date.now() / 1000;
			}

			// Do the update for every entity.
			for (const entity of this._entities) {
				entity.update(deltaTime);
			}

			// Do physics iteration for every entity.
			for (const entity of this._entities) {
				entity.doPhysics(deltaTime);
			}

			// Get the entity-entity intersections.
			const diff = Birch.Vector2.pool.get();
			for (const entity of this._entities) {
				entity.updateIntersectingEntities();
			}
			Birch.Vector2.pool.release(diff);
			for (const entity of this._entities) {
				for (const otherEntity of entity.intersectingEntities) {
					entity.onTouch(otherEntity);
				}
			}

			// Handle entity-map collisions.
			for (const entity of this._entities) {
				this._level.handleOverlappingTiles(entity);
			}
		}

		// Do the pre-render for every entity.
		for (const entity of this._entities) {
			entity.preRender();
		}

		// Do the player viewport/camera pre-render.
		for (const entry of this._players) {
			entry.value.preRender();
		}
	}

	private async _onStart(): Promise<void> {
		// Change up the UI.
		this.removeNode(this.query('.start', HTMLDivElement)!);
		this.query('.title', HTMLDivElement)!.innerHTML = `<img src="assets/sprites/title.png"></img>`;

		// Create the engine and scene.
		this._birch = new Birch.Engine(document.querySelector('.birch') as HTMLDivElement);
		this._scene = this._birch.renderer.scenes.create();

		// Load the entity factory.
		this._entityFactory = new EntityFactory(this);

		// Load the default characters, if needed.
		if (this._characterSpritePaths.length === 0) {
			await this._setupDefaultCharacterSpritePaths();
		}

		// Load the resources.
		await this._loadResources();

		// Add resize callback. Adjusts the viewport.
		this._adjustViewports = this._adjustViewports.bind(this);
		window.addEventListener('resize', this._adjustViewports);

		// Add the update callback.
		this._birch.addUpdateCallback(this._update.bind(this));

		// Create the map.
		this._level = new Level(this);

		// Run the restart function.
		this.restart();
	}

	/** Load all of the resources. */
	private _loadResources(): Promise<void[]> {
		// Setup the default load functions.
		this._birch.sounds.setDefaultLoadFunction((sound: Birch.Sound, name: string) => {
			return sound.setUrl(`assets/sounds/${name}.ogg`);
		});
		this._birch.renderer.textures.setDefaultLoadFunction((texture: Birch.Render.Texture, name: string) => {
			if (name.startsWith('blob:')) {
				return texture.setSource(name);
			}
			else {
				return texture.setSource(`assets/sprites/${name}.png`);
			}
		});

		// The list of promises to resolve before the function is done.
		const promises: Promise<void>[] = [];

		// Load the character textures.
		for (let i = 0; i < this._characterSpritePaths.length; i++) {
			promises.push(this._birch.renderer.textures.load(`${this._characterSpritePaths[i]}`));
		}

		promises.push(this._birch.renderer.textures.load(`tiles`));
		promises.concat(this._entityFactory.loadEntityResources());
		for (let i = 0; i < 4; i++) {
			promises.push(this._birch.sounds.load(`announcer${i}`));
		}
		promises.push(this._birch.sounds.load(`announcerScore`));
		promises.push(this._birch.sounds.load(`announcerSuper`));
		promises.push(this._birch.sounds.load(`go`));
		promises.push(this._birch.sounds.load(`win`));
		return Promise.all(promises);
	}

	/** Make the viewports the optimal proportions. */
	private _adjustViewports(): void {
		const numPlayers = this._players.size;
		const div = document.querySelector('div') as HTMLDivElement;
		// Cycle through every arrangement of views and find the most "square" arrangement.
		let bestCols = 1;
		let bestScore = 0;
		for (let cols = 1; cols <= numPlayers; cols++) {
			const rows = Math.ceil(numPlayers / cols);
			let score = 1;
			const aspectOfEachView = (div.clientWidth / cols) / (div.clientHeight / rows);
			// Favor 16/9 aspect ratio views.
			score /= Math.abs(aspectOfEachView - 4 / 3);
			// Favor views with no empty spaces. (Extra 0.01 is there to avoid very similar results that cause flickers because of floating-point imprecision.)
			if (numPlayers % cols === 0) {
				score *= 2.01;
			}
			// If this score is the best, record the best score and columns.
			if (score >= bestScore || cols === 1) {
				bestCols = cols;
				bestScore = score;
			}
		}
		const bestRows = Math.ceil(numPlayers / bestCols);

		// Adjust the viewports to work with the best number of columns and rows.
		let i = 0;
		for (const entry of this._players) {
			const player = entry.value;
			const row = Math.floor(i / bestCols);
			const col = i % bestCols;
			player.setViewportBounds(col / bestCols, row / bestRows, 1 / bestCols, 1 / bestRows);
			i++;
		}
	}

	private _birch!: Birch.Engine;

	private _scene!: Birch.Render.Scene;

	private _entityFactory!: EntityFactory;

	private _level!: Level;

	private _players: Birch.FastMap<number, Player> = new Birch.FastMap();

	private _entities: Birch.FastOrderedSet<Entity> = new Birch.FastOrderedSet();

	private _entitiesToRemove: Birch.FastOrderedSet<Entity> = new Birch.FastOrderedSet();

	private _announcerTime: number = 0;

	/** The total number of items currently in the game. */
	private _numItems = 0;

	/** The list of character sprites that the player can choose from. */
	private _characterSpritePaths: string[] = [];
}

ArenaSplodeApp.html = /* html */`
	<div class="title"></div>	
	<div class="birch"></div>
	<div class="message"><span>Loading...</span></div>
	<div class="start">
		<img src="assets/sprites/title.png"></img>
		<div class="custom-characters" ondrop="_dragAndDropCharacters" ondragover="_dragOverCharacters">Drag A Folder of Custom Characters Here!</div>
		<button onclick="_onStart">Start</button>
	</div>
	<div class="win">
		<img class="character"></img>
		<div class="press-y">Press <span>Y</span> on the controller to Play Again!</div>
	</div>
	`;

ArenaSplodeApp.css = /* css */`
	html, body {
		width: 100%;
		height: 100vh;
		background: black;
		color: white;
		display: grid;
		grid-template-rows: 3rem 1fr;
		grid-template-areas: "header" "main";
	}
	.title {
		grid-area: header;
		text-align: center;
	}
	.title img {
		line-height: 3rem;
		height: 2.5rem;
		margin: .25rem 0;
	}
	.birch {
		grid-area: main;
	}
	.start, .message, .win {
		grid-area: main;
		width: 100%;
		background: black;
		z-index: 1;
		text-align: center;
	}
	.message span {
		position: absolute;
		top: 50%;
		left: 50%;
		width: 100%;
		transform: translate(-50%, -50%);
		font-size: 8vw;
		color: white;
	}
	.start {
		display: grid;
		grid-template-rows: 1fr 1fr 1fr;
	}
	.start img {
		margin: auto;
		width: 50%;
		height: auto;
	}
	.start button {
		margin: auto;
		width: 25rem;
		font-size: 5rem;
		color: white;
		background: darkgreen;
		border: 4px solid white;
		border-radius: 6rem;
	}
	.start .custom-characters {
		margin: auto;
		width: 20rem;
		height: 10rem;
		border: 5px dotted white;
		display: flex;
		align-items: center;
		justify-content: center;
		font-size: 2rem;
	}
	.win {
		display: none;
		background: url('assets/sprites/score.png');
		background-size: 5rem;
	}
	.win span {
		display: inline-block;
		background: yellow;
		border: 5px solid black;
		border-radius: 3rem;
		padding: 0 1.5rem;
	}
	.win .character {
		position: absolute;
		top: 45%;
		transform: translate(-50%, -50%);
		image-rendering: crisp-edges;
		filter: drop-shadow(0 0 2rem black);
		width: 256px;
		height: 256px;
	}
	.win .press-y {
		position: absolute;
		left: 0;
		right: 0;
		bottom: 10%;
		font-size: 5rem;
		z-index: 2;
		-webkit-text-stroke: .25rem black;
		filter: drop-shadow(0 0 1rem black);
		font-weight: 900;
	}
	.viewports {
		left: 0;
		top: 0;
		width: 100%;
		height: 100%;
	}
	.viewports > div {
		border: 1px solid white;
	}
	.character-selection {
		position: absolute;
		top: 50%;
		left: 50%;
		height: 66px;
		transform: translate(-50%, -50%);
		overflow: hidden;
	}
	.character {
		width: 64px;
		height: 64px;
		border: 1px solid transparent;
	}
	.character.selected { 
		border: 1px solid white;
	}
	.stats {
		display: grid;
		margin: 0.5rem;
		height: 2rem;
		background: black;
		border: 1px solid white;
		border-radius: 1rem;
		grid-template-columns: 20rem 1fr;
	}
	.stats .score {
		background: url('assets/sprites/score.png');
		width: 0;
	}
	.stats .health {
		border-left: 1px solid white;
		border-top-right-radius: 1rem;
		border-bottom-right-radius: 1rem;
		background: green;
		height: calc(2rem - 2px);
	}
	`;

ArenaSplodeApp.setAppClass();
ArenaSplodeApp.register();

// Typing to ensure TypeScript is happy with the app global.
declare global {
	interface Window {
		Birch: typeof Birch;
	}
}
window.Birch = Birch;
