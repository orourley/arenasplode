import { ArenaSplodeApp } from 'app';
import { Birch } from 'birch';
import { Tile } from 'tile';
import { Character } from './character';
import { Entity } from './entity';
import { Projectile } from './projectile';

export class DrunkBullet extends Projectile {
	/** The constructor. */
	constructor(app: ArenaSplodeApp) {
		super(app);
		this.setFriction(2);
		this.setBounciness(.75);
		this.setAngularVelocity(16 * (2 * Math.random() - 1));
		this.sprite.setTextureName('items/drunk-bullet');
	}

	override update(_deltaTime: number): void {
		if (this.velocity.normSq < 0.1) {
			this.app.removeAndDestroyEntity(this);
		}
	}

	override onTouch(entity: Entity): void {
		// If it hits another player,
		if (entity instanceof Character && this.playerIndex !== entity.playerIndex) {
			// Play the drink sound.
			const sound = this.app.birch.sounds.get(`drunk-bullet-drink`);
			sound.play();
			this.app.birch.sounds.release(sound);
			// Make the player drunk.
			entity.addStatus('drunk', 10, this.playerIndex);
			// Destroy it.
			this.app.removeAndDestroyEntity(this);
		}
	}

	override onOverTile(tileCoords: Birch.Vector2): void {
		// If it's a wall, play a sound.
		const tile = this.app.level.getTile(tileCoords);
		if (tile === undefined) {
			return;
		}
		if (tile.type === Tile.Type.Wall) {
			// Play the crash sound.
			const sound = this.app.birch.sounds.get(`drunk-bullet-bounce`);
			sound.play();
			this.app.birch.sounds.release(sound);
			// Spin randomly.
			this.setAngularVelocity(16 * (2 * Math.random() - 1));
		}
		super.onOverTile(tileCoords);
	}

	/** Load the resources needed for the entity. */
	static override loadResources(engine: Birch.Engine): Promise<void>[] {
		return [
			engine.renderer.textures.load(`items/drunk-bullet`),
			engine.sounds.load(`drunk-bullet-bounce`),
			engine.sounds.load(`drunk-bullet-drink`)
		];
	}
}
