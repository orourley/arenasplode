import { ArenaSplodeApp } from 'app';
import { Birch } from 'birch';
import { Melee } from './melee';

export class Sword extends Melee {
	/** The constructor. */
	constructor(app: ArenaSplodeApp) {
		super(app, 'sword', 1, 1.2);
	}

	/** Sets the durability sprite. */
	override setDurability(durability: number): void {
		this.sprite.setTextureName(`items/sword${Math.min(2, Math.floor(3 * (1 - durability)))}`);
		super.setDurability(durability);
	}

	/** Load the resources needed for the entity. */
	static override loadResources(engine: Birch.Engine): Promise<void>[] {
		const promises: Promise<void>[] = [];
		for (let i = 0; i < 3; i++) {
			promises.push(engine.renderer.textures.load(`items/sword${i}`));
			promises.push(engine.sounds.load(`sword${i}`));
		}
		return promises;
	}
}
