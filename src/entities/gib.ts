import { ArenaSplodeApp } from 'app';
import { Birch } from 'birch';
import { Character } from './character';
import { Entity } from './entity';

export class Gib extends Entity {
	/** The constructor. */
	constructor(app: ArenaSplodeApp, filename: string) {
		super(app, 1);
		this.setBaseRadius(0.2);
		this.sprite.setTextureName(filename);
		this.sprite.setTint(new Birch.Color(0.25, 0.25, 0.25, 1.0));
	}

	setNecromancer(necromancer: Character | undefined): void {
		this._necromancer = necromancer;
		this._timeLeftToLive = 30;
	}

	override update(deltaTime: number): void {
		// Check if there's a necromancer controlling this gib.
		if (this._necromancer !== undefined) {
			// If the necromancer has been destroyed, then stop being controlled.
			if (this._necromancer.destroyed) {
				this._necromancer = undefined;
				return;
			}
			// If it's run its course, stop being controlled.
			this._timeLeftToLive -= deltaTime;
			if (this._timeLeftToLive <= 0) {
				this._necromancer = undefined;
				return;
			}
			// Find the closest character other than the necromancer.
			const players = this.app.players;
			let closestCharacter: Character | undefined = undefined;
			let closestDistance = Number.POSITIVE_INFINITY;
			for (const entry of players) {
				const character = entry.value.character;
				if (character !== undefined && character !== this._necromancer) {
					const distance = this.position.distance(character.position);
					if (distance < closestDistance && distance < 5) {
						closestDistance = distance;
						closestCharacter = character;
					}
				}
			}
			// If we found no other character, target the necromancer.
			if (closestCharacter === undefined) {
				closestCharacter = this._necromancer;
			}
			const diff = Birch.Vector2.pool.get();
			diff.sub(closestCharacter.position, this.position);
			// Head toward the target.
			if (diff.norm > this.radius + closestCharacter.radius) {
				diff.setNorm(1);
				diff.set(diff.x + (2 * Math.random() - 1), diff.y + (2 * Math.random() - 1));
				this.push(diff);
			}
			// Harm the target, if near enough.
			else if (this._necromancer !== closestCharacter) {
				closestCharacter.harm(this._necromancer.playerIndex, this.scale * 0.03 * deltaTime);
				if (this._timeSinceSound > 2 + Math.random()) {
					const soundIndex = Math.floor(Math.random() * 3);
					const sound = this.app.birch.sounds.get(`zombie${soundIndex}`);
					sound.play();
					this.app.birch.sounds.release(sound);		
					this._timeSinceSound = 0;
				}
			}
			Birch.Vector2.pool.release(diff);
			this._timeSinceSound += deltaTime;
		}
	}

	override onTouch(entity: Entity): void {
		if (entity instanceof Character) {
			if (this._necromancer === undefined) {
				const impulse = Birch.Vector2.pool.get();
				impulse.sub(this.position, entity.position);
				impulse.setNorm(1);
				this.push(impulse);
				Birch.Vector2.pool.release(impulse);
			}
		}
	}

	/** Load the resources needed for the entity. */
	static override loadResources(engine: Birch.Engine): Promise<void>[] {
		return [
			engine.sounds.load('zombie0'),
			engine.sounds.load('zombie1'),
			engine.sounds.load('zombie2')
		];
	}

	private _necromancer: Character | undefined;

	private _timeLeftToLive = 0;

	private _timeSinceSound = 0;
}
