import { ArenaSplodeApp } from 'app';
import { Birch } from 'birch';
import { Explosion } from './explosion';
import { Item } from './item';

export class Nuke extends Item {
	/** The constructor. */
	constructor(app: ArenaSplodeApp) {
		super(app);
		this.sprite.setTextureName('items/nuke');
	}

	override trigger(): void {
		const explosion = new Explosion(this.app);
		explosion.setPosition(this.position);
		explosion.setFinalScale(this.scale * 20);
		explosion.setPlayerIndex(this.heldBy?.playerIndex);
		this.app.addEntity(explosion);
		this.app.removeAndDestroyEntity(this);
	}

	/** Load the resources needed for the entity. */
	static override loadResources(engine: Birch.Engine): Promise<void>[] {
		return [
			engine.renderer.textures.load(`items/nuke`),
		];
	}
}
