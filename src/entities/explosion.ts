import { ArenaSplodeApp } from 'app';
import { Birch } from 'birch';
import { Character } from './character';
import { Entity } from './entity';

export class Explosion extends Entity {
	/** The constructor. */
	constructor(app: ArenaSplodeApp) {
		super(app, 4);
		this.setBaseRadius(1);
		this.setBounciness(-1);
		this.sprite.setTextureName('items/explosion');

		const sound = this.app.birch.sounds.get(`explosion`);
		sound.play();
		this.app.birch.sounds.release(sound);

		// Set the spawn time for timing the explosion.
		this._spawnTime = Date.now() / 1000;
	}

	get playerIndex(): number | undefined {
		return this._playerIndex;
	}

	setPlayerIndex(playerIndex: number | undefined): void {
		this._playerIndex = playerIndex;
	}

	/** Gets the final scale for the explosion. */
	get finalScale(): number {
		return this._finalScale;
	}

	/** Sets the final scale for the explosion. */
	setFinalScale(finalScale: number): void {
		this._finalScale = finalScale;
	}

	override onTouch(entity: Entity): void {
		const impulse = Birch.Vector2.pool.get();
		impulse.sub(entity.position, this.position);
		if (entity instanceof Character) {
			let harmFactor = 0.25;
			if (entity.playerIndex === this._playerIndex) {
				harmFactor /= 4;
			}
			entity.harm(this._playerIndex, this.radius * harmFactor / (1.0 + impulse.norm));
		}
		impulse.setNorm(10);
		entity.push(impulse);
		Birch.Vector2.pool.release(impulse);
	}

	override update(_deltaTime: number): void {
		const now = Date.now() / 1000;
		const totalTime = Math.log2(this._finalScale + 1) * 0.3;
		if (now - this._spawnTime < totalTime) {
			this.setScale(Birch.Num.lerp(0, this._finalScale, (now - this._spawnTime) / totalTime));
		}
		else {
			this.app.removeAndDestroyEntity(this);
		}
	}

	/** Load the resources needed for the entity. */
	static override loadResources(engine: Birch.Engine): Promise<void>[] {
		return [
			engine.renderer.textures.load(`items/explosion`),
			engine.sounds.load(`explosion`)
		];
	}

	/** The time when the explosion was created. */
	private _spawnTime: number;

	/** The index of the player who created this. */
	private _playerIndex: number | undefined;

	/** The final radius of the explosion. */
	private _finalScale = 1;
}
