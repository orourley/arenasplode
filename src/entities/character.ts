import { Birch } from 'birch';
import { Entity } from './entity';
import { Nuke } from './nuke';
import { Gun } from './gun';
import { ArenaSplodeApp } from 'app';
import { Gib } from './gib';
import { Stick } from './stick';
import { Melee } from './melee';
import { Item } from './item';
import { Sprite } from 'sprite';

export class Character extends Entity {
	constructor(app: ArenaSplodeApp, playerIndex: number, name: string) {
		super(app, 3);

		// Set the player index.
		this._playerIndex = playerIndex;

		// Set the name.
		this._name = name;

		// Set the sprite texture.
		this.sprite.setTextureName(name);

		// Set up physical properties.
		this.setBounciness(0.2);

		// Set the file name for the gib pieces.
		this._gibName = name;

		// Give them a stick to start with.
		const stick = new Stick(this.app);
		this.setHoldingItem(stick);
		stick.setHeldBy(this);
		this.app.addEntity(stick);
	}

	override destroy(): void {
		if (this._drunkSprite !== undefined) {
			this._drunkSprite.destroy();
		}
		super.destroy();
	}

	get playerIndex(): number {
		return this._playerIndex;
	}

	/** Gets the name. */
	get name(): string {
		return this._name;
	}

	get holdingItem(): Item | undefined {
		return this._holdingItem;
	}

	setHoldingItem(holdingItem: Item | undefined): void {
		this._holdingItem = holdingItem;
	}

	get swinging(): boolean {
		return this._swinging;
	}

	get health(): number {
		return this._health;
	}

	get drunkRotation(): number {
		return this._drunkRotation;
	}

	getStatusCount(status: string): number {
		let count = 0;
		for (let i = 0; i < this._statuses.length; i++) {
			if (this._statuses[i].name === status) {
				count += 1;
			}
		}
		return count;
	}

	addStatus(status: string, duration: number, playerCaused: number | undefined): void {
		// Set the new status effects.
		if (status === 'ghost') {
			if (this.getStatusCount('ghost') === 0) {
				this.setBounciness(-1);
				this.sprite.setTint(new Birch.Color(1, 1, 1, 0.5));
			}
		}
		else if (status === 'shrink') {
			const factor = 2;
			this.setScale(this.scale / factor);
			this.setMass(this.mass / factor);
			this.setMaxSpeed(this._maxSpeed * factor);
		}
		else if (status === 'grow') {
			if (this.getStatusCount('grow') < 2) {
				const factor = 4;
				this.setScale(this.scale * factor);
				this.setMass(this.mass * factor);
				this.setFriction(this.friction / factor);
				this.setMaxSpeed(this._maxSpeed / factor);
			}
		}
		else if (status === 'drunk') {
			if (this.getStatusCount('drunk') === 0) {
				this._drunkSprite = new Sprite(this.app.birch, this.app.scene, 5);
				this._drunkSprite.setTextureName('items/drunk-status');
				this._drunkSprite.setTint(new Birch.Color(1, 1, 1, 0.5));
				this._drunkRotation = 0;
			}
		}
		this._statuses.push(new Status(status, duration, playerCaused));
	}

	protected setMaxSpeed(maxSpeed: number): void {
		this._maxSpeed = maxSpeed;
	}

	incNumKills(): void {
		this._numKills += 1;
		if (this._numKills % 3 == 0) {
			const i = Math.floor(Math.random() * 3);
			const multikillSound = this.app.birch.sounds.get(`multikill${i}`);
			multikillSound.play();
			this.app.birch.sounds.release(multikillSound);
		}
	}

	useHeldItem(): void {
		if (this._holdingItem instanceof Melee) {
			this._swinging = true;
			this._heldOrientationOffset = Math.PI / 4;
			this._heldRadiusOffset = this.radius * .5;
		}
		else if (this._holdingItem !== undefined) {
			this._holdingItem.trigger();
		}
	}

	/** Harms this character, with damage done by the playerIndex player. */
	harm(playerIndex: number | undefined, amount: number): void {
		if (this._health === 0) {
			return;
		}
		// Reduce the health.
		if (amount > this._health) {
			amount = this._health;
		}
		this._health -= amount;
		this.app.getPlayer(this._playerIndex)!.updateHealthBar();
		// Apply a harm sound.
		if (amount >= .01 && Date.now() / 1000 - this._harmSoundTime > .125) {
			const variant = Math.floor(3 * Math.random());
			if (this._health > 0) {
				const hurtSound = this.app.birch.sounds.get(`hurt${variant}`);
				hurtSound.play();
				this.app.birch.sounds.release(hurtSound);
			}
			this._harmSoundTime = Date.now() / 1000;
		}
		// Adjust the scores.
		if (playerIndex !== undefined && playerIndex !== this._playerIndex) {
			const otherPlayer = this.app.getPlayer(playerIndex)!;
			otherPlayer.addScore(amount * 5);
			if (this._health === 0) {
				otherPlayer.addScore(5);
				otherPlayer.character?.incNumKills();
			}
		}
		if (this._health === 0) {
			// Play the death sound.
			const variant = Math.floor(3 * Math.random());
			const deathSound = this.app.birch.sounds.get(`death${variant}`);
			deathSound.play();
			this.app.birch.sounds.release(deathSound);
			// Drop any item held.
			this.dropHeldItem();
			// Add some gibs.
			for (let i = 0; i < 4; i++) {
				const gib = new Gib(this.app, this._gibName);
				gib.setScale(this.scale);
				gib.setPosition(this.position);
				const gibVelocity = Birch.Vector2.pool.get();
				gibVelocity.setX(10 * (Math.random() * 2 - 1));
				gibVelocity.setY(10 * (Math.random() * 2 - 1));
				gibVelocity.add(gibVelocity, this.velocity);
				gib.setVelocity(gibVelocity);
				Birch.Vector2.pool.release(gibVelocity);
				gib.setAngularVelocity(15 * (Math.random() * 2 - 1));
				this.app.addEntity(gib);
			}
			// Notify the player that the character died.
			this.app.getPlayer(this._playerIndex)!.die();
		}
	}

	/** Drop a held item. */
	dropHeldItem(): void {
		if (this._holdingItem !== undefined) {
			const heldItem = this._holdingItem;
			// Set the item as not held.
			this._holdingItem = undefined;
			heldItem.setHeldBy(undefined);
			// Do the toss physics.
			const newVelocity = Birch.Vector2.pool.get();
			newVelocity.rot(Birch.Vector2.UnitX, this.rotation);
			newVelocity.mult(newVelocity, 5.0);
			heldItem.push(newVelocity);
			Birch.Vector2.pool.release(newVelocity);
		}
	}

	/** Pick up and item or drop the equipped one. */
	pickUpOrDrop(): void {
		// Dropping.
		if (this.holdingItem !== undefined) {
			this.dropHeldItem();
		}
		// Picking up.
		else {
			// Get the nearest entity intersecting with the character that can be picked up.
			let nearestIntersectingEntity: Item | undefined = undefined;
			let nearestIntersectingDistance = Number.POSITIVE_INFINITY;
			for (const intersectingEntity of this.intersectingEntities) {
				// If it can be held and isn't currently held.
				if (intersectingEntity instanceof Item && intersectingEntity.heldBy === undefined) {
					const distance = this.position.distance(intersectingEntity.position);
					if (distance < nearestIntersectingDistance) {
						nearestIntersectingDistance = distance;
						nearestIntersectingEntity = intersectingEntity;
					}
				}
			}
			// If there was an valid holdable entity, do it.
			if (nearestIntersectingEntity !== undefined) {
				this.setHoldingItem(nearestIntersectingEntity);
				nearestIntersectingEntity.setHeldBy(this);
			}
		}
	}

	override update(deltaTime: number): void {
		// Run the entity update.
		super.update(deltaTime);
		// Updated the statuses.
		for (let i = 0; i < this._statuses.length; i++) {
			const status = this._statuses[i];
			status.duration -= deltaTime;
			if (status.duration <= 0) {
				this._statuses.splice(i, 1);
				i -= 1;
				// Remove the effects of the status.
				if (status.name === 'ghost') {
					if (this.getStatusCount('ghost') === 0) {
						this.setBounciness(0.2);
						this.sprite.setTint(Birch.Color.White);
					}
				}
				else if (status.name === 'shrink') {
					const factor = 2;
					this.setScale(this.scale * factor);
					this.setMass(this.mass * factor);
					this.setMaxSpeed(this._maxSpeed / factor);
				}
				else if (status.name === 'grow') {
					if (this.getStatusCount('grow') < 2) {
						const factor = 4;
						this.setScale(this.scale / factor);
						this.setMass(this.mass / factor);
						this.setFriction(this.friction * factor);
						this.setMaxSpeed(this._maxSpeed * factor);
					}
				}
				if (status.name === 'drunk') {
					if (this.getStatusCount('drunk') === 0) {
						this._drunkSprite!.destroy();
						this._drunkSprite = undefined;
						this._drunkRotation = 0;
					}
				}
			}
		}
		// Make sure the character is at or below the max speed.
		if (this.velocity.norm > this._maxSpeed) {
			const newVelocity = Birch.Vector2.pool.get();
			newVelocity.copy(this.velocity);
			newVelocity.setNorm(this._maxSpeed);
			this.setVelocity(newVelocity);
			Birch.Vector2.pool.release(newVelocity);
		}
		// If swinging, move the held item.
		if (this._swinging) {
			this._heldOrientationOffset -= 2 * Math.PI * deltaTime;
			if (this._heldOrientationOffset < -Math.PI / 4) {
				this._swinging = false;
				this._heldOrientationOffset = 0;
				this._heldRadiusOffset = 0;
			}
		}
		// If drunk, update the drunk vector. It rotates up to 1 rad every second.
		for (let i = 0; i < this._statuses.length; i++) {
			const status = this._statuses[i];
			if (status.name === 'drunk') {
				this._drunkRotation += 4 * deltaTime * (2 * Math.random() - 1);
				this.harm(status.playerCaused, deltaTime / 80.0);
			}
		}
	}

	override preRender(): void {
		super.preRender();
		if (this._holdingItem !== undefined) {
			this._holdingItem.setScale(this.scale);
			const heldPosition = Birch.Vector2.pool.get();
			heldPosition.rot(Birch.Vector2.UnitX, this.rotation + this._heldOrientationOffset);
			heldPosition.mult(heldPosition, this.radius + this._heldRadiusOffset);
			heldPosition.add(this.position, heldPosition);
			this._holdingItem.setPosition(heldPosition);
			this._holdingItem.setVelocity(this.velocity);
			Birch.Vector2.pool.release(heldPosition);
			this._holdingItem.setRotation(this.rotation + this._heldOrientationOffset);
		}
		if (this._drunkSprite !== undefined) {
			this._drunkSprite.setPosition(this.position);
			this._drunkSprite.setRotation(this._drunkRotation);
			this._drunkSprite.setScale(this.scale);
		}
	}

	/** Load the resources needed for the entity. */
	static override loadResources(engine: Birch.Engine): Promise<void>[] {
		const promises: Promise<void>[] = [];
		for (let i = 0; i < 3; i++) {
			promises.push(engine.sounds.load(`hurt${i}`));
			promises.push(engine.sounds.load(`death${i}`));
			promises.push(engine.sounds.load(`multikill${i}`));
		}
		promises.push(engine.renderer.textures.load('items/drunk-status'));
		return promises;
	}

	static characterSpriteList: string[] = [];

	private _playerIndex: number;
	private _name: string;

	private _holdingItem: Item | undefined;
	private _heldOrientationOffset: number = 0;
	private _heldRadiusOffset: number = 0;
	private _swinging: boolean = false;

	private _maxSpeed = 20;

	private _health: number = 1;
	private _harmSoundTime: number = 0;
	private _numKills: number = 0;
	private _gibName: string = '';

	/** The list of statuses. */
	private _statuses: Status[] = [];

	private _drunkRotation = 0;
	private _drunkSprite: Sprite | undefined;
}

class Status {
	constructor(name: string, duration: number, playerCaused: number | undefined) {
		this.name = name;
		this.duration = duration;
		this.playerCaused = playerCaused;
	}

	name: string;
	duration: number;
	playerCaused: number | undefined;
}