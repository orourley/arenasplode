import { ArenaSplodeApp } from 'app';
import { Birch } from 'birch';
import { Tile } from 'tile';
import { Character } from './character';
import { Entity } from './entity';
import { Item } from './item';

export class Melee extends Item {
	/** The constructor. */
	constructor(app: ArenaSplodeApp, name: string, pushBack: number, harm: number) {
		super(app);
		this._name = name;
		this._pushBack = pushBack;
		this._harm = harm;
		this.sprite.setTextureName(`items/${name}0`);
	}

	override onTouch(entity: Entity): void {
		if (entity instanceof Character && this.heldBy !== undefined && this.heldBy !== entity && this.heldBy.swinging) {
			// Hurt the entity.
			entity.harm(this.heldBy.playerIndex, this._harm * this.app.birch.deltaTime);
			// Decrease the durability of the item.
			this.setDurability(this.durability - 0.25 * this._harm * this.app.birch.deltaTime);
		}
		else if (entity instanceof Melee && this.heldBy !== undefined && entity.heldBy !== undefined && this.heldBy !== entity.heldBy && this.heldBy.swinging) {
			this._playSound();
			this.pushBack(this.heldBy, this._pushBack);
		}
	}

	override onOverTile(tileCoords: Birch.Vector2): void {
		const tile = this.app.level.getTile(tileCoords);
		if (tile !== undefined && tile.type === Tile.Type.Wall) {
			if (this.heldBy !== undefined && this.heldBy.swinging) {
				this._playSound();
			}
		}
		super.onOverTile(tileCoords);
	}

	private _playSound(): void {
		if (Date.now() / 1000 - this._soundTime > .125) {
			const variant = Math.floor(3 * Math.random());
			const sound = this.app.birch.sounds.get(`${this._name}${variant}`);
			sound.play();
			this.app.birch.sounds.release(sound);
			this._soundTime = Date.now() / 1000;
		}
	}

	private _name: string;

	private _pushBack: number;

	private _harm: number;

	private _soundTime: number = 0;
}
