import { ArenaSplodeApp } from 'app';
import { Birch } from 'birch';
import { Melee } from './melee';

export class Stick extends Melee {
	/** The constructor. */
	constructor(app: ArenaSplodeApp) {
		super(app, 'stick', 0, 0.60);
		this.setDurability(Number.POSITIVE_INFINITY);
	}

	/** The update function. */
	override update(deltaTime: number): void {
		// Remove the item if on the ground for more than 3 seconds.
		if (this.timeOnGround > 3) {
			this.app.removeAndDestroyEntity(this);
		}

		super.update(deltaTime);
	}

	/** Load the resources needed for the entity. */
	static override loadResources(engine: Birch.Engine): Promise<void>[] {
		const promises: Promise<void>[] = [];
		for (let i = 0; i < 3; i++) {
			promises.push(engine.renderer.textures.load(`items/stick${i}`));
			promises.push(engine.sounds.load(`stick${i}`));
		}
		return promises;
	}
}
