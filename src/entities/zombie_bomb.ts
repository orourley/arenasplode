import { ArenaSplodeApp } from 'app';
import { Birch } from 'birch';
import { Gib } from './gib';
import { Item } from './item';

export class ZombieBomb extends Item {
	/** The constructor. */
	constructor(app: ArenaSplodeApp) {
		super(app);
		this.sprite.setTextureName('items/zombie');
	}

	override trigger(): void {
		const entities = this.app.getEntities();
		for (const entity of entities) {
			if (entity instanceof Gib && entity.position.distance(this.position) < 10) {
				const sound = this.app.birch.sounds.get(`zombie`);
				sound.play();
				this.app.birch.sounds.release(sound);		
				entity.setNecromancer(this.heldBy);
			}
		}
		this.app.removeAndDestroyEntity(this);
	}

	/** Load the resources needed for the entity. */
	static override loadResources(engine: Birch.Engine): Promise<void>[] {
		return [
			engine.renderer.textures.load(`items/zombie`),
			engine.sounds.load('zombie')
		];
	}
}
