import { ArenaSplodeApp } from 'app';
import { Birch } from 'birch';
import { Character } from 'entities/character';
import { Sprite } from 'sprite';
import { Tile } from 'tile';
import { FastOrderedSet } from '../../../birch/src/internal';

export class Entity {
	/** The constructor.
	  * @param layer - The map is at 0, items are at 1, held items are at 2, players are at 3. */
	constructor(app: ArenaSplodeApp, layer: number) {
		this._app = app;
		this._sprite = new Sprite(app.birch, app.scene, layer);
		this._app.level.addToLevel(this);
	}

	/** The destructor. */
	destroy(): void {
		this._sprite.destroy();
		for (const entity of this.intersectingEntities) {
			entity.intersectingEntities.remove(this);
		}
		this._app.level.removeFromLevel(this);
		this._destroyed = true;
	}

	/** Returns true if the entity is destroyed. */
	get destroyed(): boolean {
		return this._destroyed;
	}

	/** Gets the position. */
	get position(): Birch.Vector2Readonly {
		return this._position;
	}

	/** Sets the position. */
	setPosition(position: Birch.Vector2Readonly): void {
		this._app.level.removeFromLevel(this);
		this._position.copy(position);
		this._app.level.addToLevel(this);
		this._sprite.setPosition(position);
	}

	/** Gets the rotation in radians. */
	get rotation(): number {
		return this._rotation;
	}

	/** Sets the rotation in radians. */
	setRotation(rotation: number): void {
		this._rotation = rotation;
		this._sprite.setRotation(rotation);
	}

	/** Gets the scale. */
	get scale(): number {
		return this._scale;
	}

	/** Sets the scale. */
	setScale(scale: number): void {
		this._app.level.removeFromLevel(this);
		this._scale = scale;
		this._app.level.addToLevel(this);
		this._sprite.setScale(2 * this._baseRadius * this._scale);
	}

	/** Gets the velocity in units per second. */
	get velocity(): Birch.Vector2Readonly {
		return this._velocity;
	}

	/** Sets the velocity in units per second. */
	setVelocity(velocity: Birch.Vector2Readonly): void {
		this._velocity.copy(velocity);
	}

	/** Gets the angular velocity in radians per second. */
	get angularVelocity(): number {
		return this._angularVelocity;
	}

	/** Sets the angular velocity in radians per second. */
	setAngularVelocity(angularVelocity: number): void {
		this._angularVelocity = angularVelocity;
	}

	/** Gets the radius. */
	get radius(): number {
		return this._baseRadius * this._scale;
	}

	/** Sets the radius. */
	setBaseRadius(baseRadius: number): void {
		this._app.level.removeFromLevel(this);
		this._baseRadius = baseRadius;
		this._app.level.addToLevel(this);
		this._sprite.setScale(2 * this._baseRadius * this._scale);
	}

	/** Gets the mass. */
	get mass(): number {
		return this._mass;
	}

	/** Sets the mass. */
	setMass(mass: number): void {
		this._mass = mass;
	}

	/** Gets the friction. */
	get friction(): number {
		return this._friction;
	}

	/** Sets the friction. */
	setFriction(friction: number): void {
		this._friction = friction;
	}

	/** Gets the bounciness. */
	get bounciness(): number {
		return this._bounciness;
	}

	/** Sets the bounciness. A negative number means it doesn't bound off of walls. */
	setBounciness(bounciness: number): void {
		this._bounciness = bounciness;
	}

	/** Gets the sprite. */
	protected get sprite(): Sprite {
		return this._sprite;
	}

	/** Gets the app. */
	protected get app(): ArenaSplodeApp {
		return this._app;
	}

	/** The update function. */
	update(_deltaTime: number): void {
	}

	/** The pre-render function. */
	preRender(): void {
		this._sprite.setPosition(this._position);
		this._sprite.setRotation(this._rotation);
	}

	/** Does the iteraction physics. */
	doPhysics(deltaTime: number): void {
		// Max speed.
		if (this._velocity.normSq > 10000000000) {
			this._velocity.setNorm(100000);
		}
		// Friction
		const velocityNorm = this._velocity.norm;
		if (this._friction * deltaTime < velocityNorm) {
			this._velocity.setNorm(velocityNorm - this._friction * deltaTime);
		}
		else {
			this._velocity.set(0, 0);
		}
		if (this._friction * deltaTime < Math.abs(this._angularVelocity)) {
			this._angularVelocity -= Math.sign(this._angularVelocity) * this._friction * deltaTime;
		}
		else {
			this._angularVelocity = 0;
		}
		// Apply the velocity and angular velocity.
		if (this._velocity.normSq > 0) {
			this._app.level.removeFromLevel(this);
			this._position.addMult(this._position, 1, this._velocity, deltaTime);
			this._app.level.addToLevel(this);
		}
		this._rotation += this._angularVelocity * deltaTime;
	}

	onTouch(_entity: Entity): void {
	}

	onOverTile(tileCoords: Birch.Vector2): void {
		// If it's a wall, move it away.
		const tile = this._app.level.getTile(tileCoords);
		if (tile === undefined) {
			return;
		}
		if (this._bounciness >= 0 && tile.type === Tile.Type.Wall && this.radius <= 0.5) {
			const offset = Birch.Vector2.pool.get();
			const contains = this.getClosestPoint(offset, tileCoords);
			offset.sub(this._position, offset);
			const offsetNorm = offset.norm;
			if (0 < offsetNorm && offsetNorm < this._baseRadius * this._scale) {
				if (contains) {
					offset.mult(offset, (-this._baseRadius * this._scale - offsetNorm) / offsetNorm);
				}
				else {
					offset.mult(offset, (this._baseRadius * this._scale - offsetNorm) / offsetNorm);
				}
				const newPosition = Birch.Vector2.pool.get();
				newPosition.add(this._position, offset);
				this.setPosition(newPosition);
				Birch.Vector2.pool.release(newPosition);
				const newVelocity = Birch.Vector2.pool.get();
				offset.normalize(offset);
				newVelocity.addMult(this._velocity, 1, offset, Math.max(0, -(1 + this._bounciness) * this._velocity.dot(offset)));
				this.setVelocity(newVelocity);
				Birch.Vector2.pool.release(newVelocity);
			}
			Birch.Vector2.pool.release(offset);
		}
	}

	/** Gets the tile edge closest to the entity. Returns true if the tile contains the entity position. */
	protected getClosestPoint(closestPoint: Birch.Vector2, tileCoords: Birch.Vector2): boolean {
		const tileBounds = Birch.Rectangle.pool.get();
		tileBounds.set(tileCoords.x, tileCoords.y, 1, 1);
		const contains = tileBounds.closest(closestPoint, this._position, false);
		Birch.Rectangle.pool.release(tileBounds);
		return contains;
	}

	/** Pushes this in a given direction. */
	push(impulse: Birch.Vector2): void {
		this._velocity.addMult(this._velocity, 1, impulse, 1.0 / this._mass);
	}

	/** Push both this and the other entity away from each other by the velocity amount. */
	pushBack(entity: Entity, amount: number): void {
		const push = Birch.Vector2.pool.get();
		push.sub(this._position, entity.position);
		push.setNorm(amount);
		this.push(push);
		push.neg(push);
		entity.push(push);
		Birch.Vector2.pool.release(push);
	}

	/** Updates the intersecting entities list. */
	updateIntersectingEntities(): void {
		// First check existing intersecting entities, and remove any that are no longer intesecting.
		const diff = Birch.Vector2.pool.get();
		for (const otherEntity of this.intersectingEntities) {
			diff.sub(this.position, otherEntity.position);
			const radii = this.radius + otherEntity.radius;
			if (diff.dot(diff) > radii * radii) { // Intersecting
				this.intersectingEntities.remove(otherEntity);
			}
		}
		// Check the rest of the tiles that this entity is, to see if there are any new entities.
		const min = Birch.Vector2.pool.get();
		const max = Birch.Vector2.pool.get();
		const level = this._app.level;
		min.set(Math.max(0, Math.floor(this._position.x - this._baseRadius * this._scale)),
			Math.max(0, Math.floor(this._position.y - this._baseRadius * this._scale)));
		max.set(Math.min(level.size.x - 1, Math.ceil(this._position.x + this._baseRadius * this._scale)),
			Math.min(level.size.y - 1, Math.ceil(this._position.y + this._baseRadius * this._scale)));
		const tiles = level.tiles;
		for (let y = min.y; y <= max.y; y++) {
			for (let x = min.x; x <= max.x; x++) {
				const entities = tiles[y][x].entities;
				for (let i = 0, l = entities.size(); i < l; i++) {
					const otherEntity = entities.getIndex(i)!;
					if (this === otherEntity) {
						continue;
					}
					diff.sub(this.position, otherEntity.position);
					const radii = this.radius + otherEntity.radius;
					if (diff.dot(diff) < radii * radii && !this.intersectingEntities.has(otherEntity)) {
						this.intersectingEntities.add(otherEntity);
					}
				}
			}
		}
		Birch.Vector2.pool.release(diff);
		Birch.Vector2.pool.release(min);
		Birch.Vector2.pool.release(max);
	}

	/** Load the resources needed for the entity. */
	static loadResources(_engine: Birch.Engine): Promise<void>[] {
		return [];
	}

	// EXISTENCE

	private _destroyed: boolean = false;

	// FRAME AND FRAME DERIVATIVES

	/** The 2D position within the world. */
	private _position: Birch.Vector2 = new Birch.Vector2();

	/** The rotation in radians. */
	private _rotation: number = 0;

	/** The scale of the entity. */
	private _scale: number = 1;

	/** The velocity of the entity. */
	private _velocity = new Birch.Vector2();

	/** The angular velocity of the entity. */
	private _angularVelocity = 0;

	// PHYSICS

	/** The base radius of the entity before any scaling. */
	private _baseRadius: number = 0.5;

	/** The mass of the entity. */
	private _mass = 1;

	/** The friction of the entity with the floor. */
	private _friction = 32;

	/** When doing collision physics, this is the spring coefficient. */
	private _bounciness = 0;

	/** The set of entities this is currently intersecting with. */
	intersectingEntities: Birch.FastOrderedSet<Entity> = new FastOrderedSet();

	// SYSTEMS

	private _app: ArenaSplodeApp;

	// SPRITE

	/** The sprite for the entity. */
	private _sprite: Sprite;
}
