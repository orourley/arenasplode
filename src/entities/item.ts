import { ArenaSplodeApp } from 'app';
import { Birch } from 'birch';
import { Character } from './character';
import { Entity } from './entity';

export class Item extends Entity {
	constructor(app: ArenaSplodeApp) {
		super(app, 1);
	}

	/** The destructor. */
	override destroy(): void {
		if (this.heldBy !== undefined) {
			this.heldBy.dropHeldItem();
		}
		super.destroy();
	}

	get timeOnGround(): number {
		return this._timeOnGround;
	}

	/** When an item is used. */
	trigger(): void {
	}

	/** The update function. */
	override update(deltaTime: number): void {
		// If any item is outside the level and not held, destroy it.
		if (this._heldBy === undefined) {
			const levelSize = this.app.level.size;
			if (this.position.x < 0 || levelSize.x <= this.position.x ||
					this.position.y < 0 || levelSize.x <= this.position.y) {
				this.app.removeAndDestroyEntity(this);
			}
		}
		// If any item has been on the ground for awhile, destroy it.
		if (this._heldBy === undefined) {
			this._timeOnGround += deltaTime;
			if (this._timeOnGround >= 30) {
				this.app.removeAndDestroyEntity(this);
			}
			else if (this._timeOnGround >= 25) {
				const blinkTime = Birch.Num.wrap(this._timeOnGround, 0, 0.25);
				// console.log(blinkTime);
				if (blinkTime < 0.125) {
					this.sprite.setTint(blinkColor);
				}
				else {
					this.sprite.setTint(Birch.Color.White);
				}
			}
		}
	}

	/** Gets the character holding this, if any. */
	get heldBy(): Character | undefined {
		return this._heldBy;
	}

	/** Sets the character holding this, if any. */
	setHeldBy(character: Character | undefined): void {
		this._heldBy = character;
		if (character !== undefined) {
			this.sprite.setLevel(2);
			this.sprite.setTint(Birch.Color.White);
		}
		else {
			this.sprite.setLevel(1);
			this._timeOnGround = 0;
		}
	}

	/** Gets the durability that the item has left. */
	get durability(): number {
		return this._durability;
	}

	/** Sets the durability that the item has left. */
	setDurability(durability: number): void {
		this._durability = durability;
		if (this._durability <= 0) {
			this.app.removeAndDestroyEntity(this);
		}
	}

	/** The character holding this, if any. */
	private _heldBy: Character | undefined;

	/** How much durability the item has left. */
	private _durability: number = 1;

	/** How much time has it been on the ground. */
	private _timeOnGround: number = 0;
}

const blinkColor = new Birch.Color(1, 1, 1, 0.5);
