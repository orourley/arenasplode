import { ArenaSplodeApp } from 'app';
import { Birch } from 'birch';
import { DrunkBullet } from './drunk_bullet';
import { Gun } from './gun';

export class DrunkGun extends Gun {
	/** The constructor. */
	constructor(app: ArenaSplodeApp) {
		super(app, 'drunk');
		this._projectileType = DrunkBullet;
		this._projectileSpeed = 5;
		this._ammo = 18;
	}

	protected override _playFireSound(): void {
		const sound = this.app.birch.sounds.get(`drunk-gun`);
		sound.play();
		this.app.birch.sounds.release(sound);
	}

	/** Load the resources needed for the entity. */
	static override loadResources(engine: Birch.Engine): Promise<void>[] {
		return [
			engine.renderer.textures.load(`items/drunk-gun`),
			engine.sounds.load(`drunk-gun`)
		];
	}
}
