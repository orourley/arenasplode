import { ArenaSplodeApp } from 'app';
import { Birch } from 'birch';
import { Entity } from './entity';
import { Character } from './character';
import { Projectile } from './projectile';

export class StickyBall extends Projectile {
	/** The constructor. */
	constructor(app: ArenaSplodeApp) {
		super(app);
		this.sprite.setLevel(4);
		this.setBounciness(0.5);
		this.sprite.setTextureName('items/sticky-ball');
	}

	override onTouch(entity: Entity): void {
		if (entity instanceof Character) {
			// Stick to the character if it isn't already stuck to another.
			if (entity instanceof Character && this.playerIndex !== entity.playerIndex && this._stuckTo === undefined) {
				this._stuckTo = entity;
				this._stuckToOffset.sub(this.position, entity.position);
				this._stuckToOffset.setNorm(this._stuckTo.radius * 0.25);
				this._stuckToLastRotation = entity.rotation;
				// Play the stuck-to sound.
				const sound = this.app.birch.sounds.get('sticky-ball');
				sound.play();
				this.app.birch.sounds.release(sound);
			}
		}
	}

	/** The update function. */
	override update(deltaTime: number): void {
		// If it's dead, remove it.
		this._timeToLive -= deltaTime;
		if (this._timeToLive <= 0) {
			this.app.removeAndDestroyEntity(this);
			return;
		}

		// If stuck to a character,
		if (this._stuckTo !== undefined) {
			// Check if the stuckTo character is dead.
			if (this._stuckTo.destroyed) {
				this._stuckTo = undefined;
				const sound = this.app.birch.sounds.get('sticky-ball');
				sound.play();
				this.app.birch.sounds.release(sound);
				return;
			}
			// See if it's still stuck to them. If not, shoot away.
			const diff = Birch.Vector2.pool.get();
			diff.sub(this.position, this._stuckTo.position);
			if (diff.norm > this.radius + this._stuckTo.radius) {
				this._stuckTo = undefined;
				diff.setNorm(10);
				this.push(diff);
				const sound = this.app.birch.sounds.get('sticky-ball');
				sound.play();
				this.app.birch.sounds.release(sound);
				return;
			}
			Birch.Vector2.pool.release(diff);
			// Harm them a little.
			this._stuckTo.harm(this.playerIndex, 0.02 * deltaTime);
			// Slow them down.
			const newVelocity = Birch.Vector2.pool.get();
			newVelocity.mult(this._stuckTo.velocity, Math.pow(0.1, deltaTime));
			this._stuckTo.setVelocity(newVelocity);
			Birch.Vector2.pool.release(newVelocity);
			// If the stuck-to character wiggles enough, it move away a bit.
			if (Math.abs((this._stuckTo.rotation - this._stuckToLastRotation) / deltaTime) > 50) {
				this._stuckToOffset.setNorm(this._stuckToOffset.norm + 5.0 * this.radius * deltaTime);
			}
			this._stuckToLastRotation = this._stuckTo.rotation;
		}

		super.update(deltaTime);
	}

	/** The pre-render function. */
	override preRender(): void {
		if (this._stuckTo !== undefined) {
			// Make this stuck to them.
			this.setVelocity(this._stuckTo.velocity);
			const newPosition = Birch.Vector2.pool.get();
			newPosition.add(this._stuckTo.position, this._stuckToOffset);
			this.setPosition(newPosition);
			Birch.Vector2.pool.release(newPosition);
		}
		super.preRender();
	}

	/** Load the resources needed for the entity. */
	static override loadResources(engine: Birch.Engine): Promise<void>[] {
		return [
			engine.renderer.textures.load('items/sticky-ball'),
			engine.sounds.load('sticky-ball')
		];
	}

	/** The number of seconds left for this to live. */
	private _timeToLive: number = 10;

	/** The character that this is stuck to. */
	private _stuckTo: Character | undefined;

	/** The offset from the character where this remains once it is attached. */
	private _stuckToOffset: Birch.Vector2 = new Birch.Vector2();

	/** The last rotation of the stuck-to character. */
	private _stuckToLastRotation: number = 0;
}
