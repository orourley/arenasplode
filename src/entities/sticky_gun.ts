import { ArenaSplodeApp } from 'app';
import { Birch } from 'birch';
import { StickyBall } from './sticky_ball';
import { Gun } from './gun';

export class StickyGun extends Gun {
	/** The constructor. */
	constructor(app: ArenaSplodeApp) {
		super(app, 'sticky');
		this._projectileType = StickyBall;
		this._projectileSpeed = 10;
		this._ammo = 32;
	}

	protected override _playFireSound(): void {
		const sound = this.app.birch.sounds.get(`bullet-gun`);
		sound.play();
		this.app.birch.sounds.release(sound);
	}

	/** Load the resources needed for the entity. */
	static override loadResources(engine: Birch.Engine): Promise<void>[] {
		return [
			engine.renderer.textures.load(`items/sticky-gun`),
			engine.sounds.load(`sticky-gun`)
		];
	}
}
