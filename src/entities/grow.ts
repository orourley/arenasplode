import { ArenaSplodeApp } from 'app';
import { Birch } from 'birch';
import { Character } from './character';
import { Entity } from './entity';
import { Item } from './item';

export class Grow extends Item {
	/** The constructor. */
	constructor(app: ArenaSplodeApp) {
		super(app);
		this.sprite.setTextureName('items/grow');
	}

	override onTouch(entity: Entity): void {
		if (entity instanceof Character) {
			entity.addStatus('grow', 10, entity.playerIndex);
			this.app.removeAndDestroyEntity(this);
			const sound = this.app.birch.sounds.get(`grow`);
			sound.play();
			this.app.birch.sounds.release(sound);
		}
	}

	/** Load the resources needed for the entity. */
	static override loadResources(engine: Birch.Engine): Promise<void>[] {
		return [
			engine.renderer.textures.load(`items/grow`),
			engine.sounds.load('grow')
		];
	}
}
