import { ArenaSplodeApp } from 'app';
import { Birch } from 'birch';
import { Bomb } from './bomb';
import { Gun } from './gun';

export class BombGun extends Gun {
	/** The constructor. */
	constructor(app: ArenaSplodeApp) {
		super(app, 'bomb');
		this._projectileType = Bomb;
		this._projectileSpeed = 5;
		this._ammo = 12;
	}

	protected override _playFireSound(): void {
		const sound = this.app.birch.sounds.get(`bomb-gun`);
		sound.play();
		this.app.birch.sounds.release(sound);
	}

	/** Load the resources needed for the entity. */
	static override loadResources(engine: Birch.Engine): Promise<void>[] {
		return [
			engine.renderer.textures.load(`items/bomb-gun`),
			engine.sounds.load(`bomb-gun`)
		];
	}
}
