import { ArenaSplodeApp } from 'app';
import { Birch } from 'birch';
import { Item } from './item';
import { Projectile } from './projectile';

export class Gun extends Item {
	/** The constructor. */
	constructor(app: ArenaSplodeApp, name: string) {
		super(app);
		this.sprite.setTextureName('items/' + name + '-gun');
	}

	override trigger(): void {
		if (this.heldBy === undefined || this._projectileType === undefined) {
			return;
		}
		if (this._ammo > 0) {
			this._playFireSound();
			const projectile = new this._projectileType(this.app);
			projectile.setPlayerIndex(this.heldBy.playerIndex);
			projectile.setScale(this.scale);
			const projectilePosition = Birch.Vector2.pool.get();
			projectilePosition.rot(Birch.Vector2.UnitX, this.heldBy.rotation);
			projectilePosition.addMult(this.heldBy.position, 1, projectilePosition, this.heldBy.radius + projectile.radius * 1.2);
			projectile.setPosition(projectilePosition);
			Birch.Vector2.pool.release(projectilePosition);
			const projectileVelocity = Birch.Vector2.pool.get();
			projectileVelocity.rot(Birch.Vector2.UnitX, this.heldBy.rotation);
			projectileVelocity.addMult(this.velocity, 1, projectileVelocity, this._projectileSpeed);
			projectile.setVelocity(projectileVelocity);
			Birch.Vector2.pool.release(projectileVelocity);
			this.app.addEntity(projectile);
			this._ammo -= 1;
		}
		else {
			const sound = this.app.birch.sounds.get(`out-of-ammo`);
			sound.play();
			this.app.birch.sounds.release(sound);
		}
	}

	/** The update function. */
	override update(deltaTime: number): void {
		// Remove the item if on the ground for more than 3 seconds.
		if (this.heldBy !== undefined || this._ammo > 0) {
			this._timeOnGroundWithoutAmmo = 0;
		}
		else {
			this._timeOnGroundWithoutAmmo += deltaTime;
			if (this._timeOnGroundWithoutAmmo > 3) {
				this.app.removeAndDestroyEntity(this);
			}
		}

		super.update(deltaTime);
	}

	protected _playFireSound(): void {
	}

	protected _projectileSpeed: number = 15;

	protected _projectileType: { new (app: ArenaSplodeApp): Projectile } | undefined;

	protected _ammo: number = 1;

	private _timeOnGroundWithoutAmmo: number = 0;
}
