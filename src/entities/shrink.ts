import { ArenaSplodeApp } from 'app';
import { Birch } from 'birch';
import { Character } from './character';
import { Entity } from './entity';
import { Item } from './item';

export class Shrink extends Item {
	/** The constructor. */
	constructor(app: ArenaSplodeApp) {
		super(app);
		this.sprite.setTextureName('items/shrink');
	}

	override trigger(): void {
		// Make every other character shrink.
		for (const entry of this.app.players) {
			const character = entry.value.character;
			if (character === undefined || character === this.heldBy) {
				continue;
			}
			character.addStatus('shrink', 10, this.heldBy!.playerIndex);
		}
		// Destroy this.
		this.app.removeAndDestroyEntity(this);
		// Play the shrink sound.
		const sound = this.app.birch.sounds.get(`shrink`);
		sound.play();
		this.app.birch.sounds.release(sound);
	}

	/** Load the resources needed for the entity. */
	static override loadResources(engine: Birch.Engine): Promise<void>[] {
		return [
			engine.renderer.textures.load(`items/shrink`),
			engine.sounds.load('shrink')
		];
	}
}
