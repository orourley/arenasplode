import { ArenaSplodeApp } from 'app';
import { Birch } from 'birch';
import { Tile } from 'tile';
import { Character } from './character';
import { Entity } from './entity';
import { Explosion } from './explosion';
import { Projectile } from './projectile';

export class Bomb extends Projectile {
	/** The constructor. */
	constructor(app: ArenaSplodeApp) {
		super(app);
		this.setBounciness(0.25);
		this.setBaseRadius(0.25);
		this.sprite.setTextureName('items/bomb');
	}

	override update(_deltaTime: number): void {
		if (this.velocity.normSq < 0.1) {
			this.app.removeAndDestroyEntity(this);
		}
	}

	override onTouch(entity: Entity): void {
		if (entity instanceof Character && this.playerIndex !== entity.playerIndex) {
			this._explode();
		}
	}

	override onOverTile(tileCoords: Birch.Vector2): void {
		// If it's a wall, explode.
		const tile = this.app.level.getTile(tileCoords);
		if (tile === undefined) {
			return;
		}
		if (tile.type === Tile.Type.Wall) {
			this._explode();
		}
	}

	private _explode(): void {
		const explosion = new Explosion(this.app);
		explosion.setPosition(this.position);
		explosion.setFinalScale(2.0 * this.scale);
		explosion.setPlayerIndex(this.playerIndex);
		this.app.addEntity(explosion);
		this.app.removeAndDestroyEntity(this);
	}

	/** Load the resources needed for the entity. */
	static override loadResources(engine: Birch.Engine): Promise<void>[] {
		return [
			engine.renderer.textures.load(`items/bomb`),
		];
	}
}
