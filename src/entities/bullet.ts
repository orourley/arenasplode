import { ArenaSplodeApp } from 'app';
import { Birch } from 'birch';
import { Tile } from 'tile';
import { Character } from './character';
import { Entity } from './entity';
import { Projectile } from './projectile';

export class Bullet extends Projectile {
	/** The constructor. */
	constructor(app: ArenaSplodeApp) {
		super(app);
		this.setBaseRadius(0.125);
		this.setBounciness(0.75);
		this.sprite.setTextureName('items/bullet');
	}

	override update(_deltaTime: number): void {
		if (this.velocity.normSq < 0.1) {
			this.app.removeAndDestroyEntity(this);
		}
	}

	override onTouch(entity: Entity): void {
		// If it hits another player,
		if (entity instanceof Character && this.playerIndex !== entity.playerIndex) {
			// Harm the player.
			if (entity instanceof Character) {
				entity.harm(this.playerIndex, 0.06);
			}
			// Push the player back.
			const impulse = Birch.Vector2.pool.get();
			impulse.sub(entity.position, this.position);
			impulse.setNorm(1);
			entity.push(impulse);
			Birch.Vector2.pool.release(impulse);
			// Destroy this.
			this.app.removeAndDestroyEntity(this);
		}
	}

	override onOverTile(tileCoords: Birch.Vector2): void {
		// If it's a wall, play a sound.
		const tile = this.app.level.getTile(tileCoords);
		if (tile === undefined) {
			return;
		}
		if (tile.type === Tile.Type.Wall) {
			const bulletSound = this.app.birch.sounds.get(`bullet`);
			bulletSound.play();
			this.app.birch.sounds.release(bulletSound);
		}
		super.onOverTile(tileCoords);
	}

	/** Load the resources needed for the entity. */
	static override loadResources(engine: Birch.Engine): Promise<void>[] {
		return [
			engine.renderer.textures.load(`items/bullet`),
			engine.sounds.load(`bullet`)
		];
	}
}
