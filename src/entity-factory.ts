import { ArenaSplodeApp } from 'app';
import { Birch } from 'birch';
import { BulletGun } from 'entities/bullet_gun';
import { BombGun } from 'entities/bomb_gun';
import { Bomb } from 'entities/bomb';
import { Bullet } from 'entities/bullet';
import { Character } from 'entities/character';
import { Entity } from 'entities/entity';
import { Explosion } from 'entities/explosion';
import { Ghost } from 'entities/ghost';
import { Gib } from 'entities/gib';
import { Gun } from 'entities/gun';
import { Nuke } from 'entities/nuke';
import { Projectile } from 'entities/projectile';
import { Stick } from 'entities/stick';
import { StickyBall } from 'entities/sticky_ball';
import { StickyGun } from 'entities/sticky_gun';
import { Sword } from 'entities/sword';
import { Shrink } from 'entities/shrink';
import { Grow } from 'entities/grow';
import { ZombieBomb } from 'entities/zombie_bomb';
import { DrunkGun } from 'entities/drunk_gun';
import { DrunkBullet } from 'entities/drunk_bullet';

/** The class that handles entities and items. */
export class EntityFactory {
	/** The constructor. */
	constructor(app: ArenaSplodeApp) {
		// Set the app.
		this._app = app;

		// Get the total weights.
		this._totalItemWeights = 0;
		for (const entry of this._itemWeights) {
			this._totalItemWeights += entry[1];
		}
	}

	/** Loads the resources for every entity in the entityTypes list. */
	loadEntityResources(): Promise<void>[] {
		const promises: Promise<void>[] = [];
		for (let i = 0; i < this._entityTypes.length; i++) {
			promises.concat(this._entityTypes[i].loadResources(this._app.birch));
		}
		promises.push(this._app.birch.sounds.load(`out-of-ammo`));
		return promises;
	}

	/** Creates a randomly chosen item, adding it at a random location within the map. */
	createRandomItem(): void {
		// Get a random aggregate weight and decrease until we find our index.
		let randomWeight = Math.floor(Math.random() * this._totalItemWeights);
		for (const entry of this._itemWeights) {
			if (randomWeight < entry[1]) {
				const itemType = entry[0];
				const item = (new (itemType as any)(this._app)) as Entity;
				this._app.addEntity(item);
				item.setPosition(new Birch.Vector2(1 + Math.random() * (this._app.level.size.x - 2), 1 + Math.random() * (this._app.level.size.y - 2)));
				break;
			}
			else {
				randomWeight -= entry[1];
			}
		}
	}

	/** Every type of entity in the game. */
	private _entityTypes = [
		BombGun,
		Bomb,
		BulletGun,
		Bullet,
		Character,
		DrunkBullet,
		DrunkGun,
		Explosion,
		Ghost,
		Gib,
		Grow,
		Gun,
		Nuke,
		Projectile,
		Shrink,
		StickyBall,
		StickyGun,
		Stick,
		Sword,
		ZombieBomb
	];

	/** The item weights for randomly choosing an item. */
	private _itemWeights = new Map<typeof Entity, number>([
		[BulletGun, 1],
		[BombGun, 1],
		[DrunkGun, 1],
		[Grow, 1],
		[Ghost, 1],
		[Nuke, 1],
		[Shrink, 1],
		[StickyGun, 1],
		[Sword, 1],
		[ZombieBomb, 1]
	]);

	private _app: ArenaSplodeApp;

	/** The sum of the item weights. */
	private _totalItemWeights = 0;
}
