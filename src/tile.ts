import { Birch } from 'birch';
import { Entity } from 'entities/entity';

export class Tile {
	type: Tile.Type = Tile.Type.Floor;
	entities: Birch.FastOrderedSet<Entity> = new Birch.FastOrderedSet();
}

export namespace Tile {
	export enum Type {
		Floor,
		Wall,
		NumTiles
	}
}
