const TerserPlugin = require('terser-webpack-plugin');
const { merge } = require('webpack-merge');
const base = require('./webpack.base.js');

module.exports = merge(base, {
	mode: 'none',
	optimization: {
		minimize: true,
		minimizer: [new TerserPlugin({
			terserOptions: {
				keep_classnames: true
		}})],
	},
});
